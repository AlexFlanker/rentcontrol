package com.example.alexflanker89.rentcontrol.domain;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity(

        active = true,

        nameInDb = "UseTable",

        createInDb = true,

        generateConstructors = true,

        generateGettersSetters = true
)
public class UseTable {
    @Id(autoincrement = true)
    private Long id;
    private Long vehicleId;
    private String name;
    private Date startTime;
    private Date endTime;
    private boolean isRent;
/** Used to resolve relations */
@Generated(hash = 2040040024)
private transient DaoSession daoSession;
/** Used for active entity operations. */
@Generated(hash = 87799794)
private transient UseTableDao myDao;
@Generated(hash = 785441829)
public UseTable(Long id, Long vehicleId, String name, Date startTime,
        Date endTime, boolean isRent) {
    this.id = id;
    this.vehicleId = vehicleId;
    this.name = name;
    this.startTime = startTime;
    this.endTime = endTime;
    this.isRent = isRent;
}
@Generated(hash = 220807335)
public UseTable() {
}
public Long getId() {
    return this.id;
}
public void setId(Long id) {
    this.id = id;
}
public Long getVehicleId() {
    return this.vehicleId;
}
public void setVehicleId(Long vehicleId) {
    this.vehicleId = vehicleId;
}
public String getName() {
    return this.name;
}
public void setName(String name) {
    this.name = name;
}
public Date getStartTime() {
    return this.startTime;
}
public void setStartTime(Date startTime) {
    this.startTime = startTime;
}
public Date getEndTime() {
    return this.endTime;
}
public void setEndTime(Date endTime) {
    this.endTime = endTime;
}
public boolean getIsRent() {
    return this.isRent;
}
public void setIsRent(boolean isRent) {
    this.isRent = isRent;
}
/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 128553479)
public void delete() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.delete(this);
}
/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 1942392019)
public void refresh() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.refresh(this);
}
/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 713229351)
public void update() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.update(this);
}
/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 484308124)
public void __setDaoSession(DaoSession daoSession) {
    this.daoSession = daoSession;
    myDao = daoSession != null ? daoSession.getUseTableDao() : null;
}

}
