package com.example.alexflanker89.rentcontrol;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.bluetooth.*;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.example.alexflanker89.rentcontrol.BLEService.BLEJob;

import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private  BluetoothAdapter.LeScanCallback callback;
    BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private  Handler mHandler;
    private static final long SCAN_PERIOD = 10000;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        callback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                if(device.getName()!=null)
                    Log.i("device",device.getName() + " " + device.getAddress() + " rssi:" + rssi);
            }
        };

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                startJob();
//                scanLeDevice(true);

            }
        });
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "not permission", Toast.LENGTH_SHORT).show();
            finish();
        }
        mHandler = new Handler();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void scanLeDevice(final boolean enable) {
        if (enable) {

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(callback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(callback);
            Toast.makeText(this,"BLE start!",Toast.LENGTH_SHORT).show();
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(callback);
            Toast.makeText(this,"BLE stoped!",Toast.LENGTH_SHORT).show();
        }


    }

    private void startJob() {
        JobScheduler jobScheduler;
        ComponentName mService = new ComponentName(getApplicationContext(), BLEJob.class);
        JobInfo jobInfo = new JobInfo.Builder(1, mService)
                .setPeriodic(10000)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .build();
        jobScheduler = (JobScheduler) getApplicationContext().getSystemService(Context.JOB_SCHEDULER_SERVICE);
        int res = jobScheduler.schedule(jobInfo);

        if (res == JobScheduler.RESULT_SUCCESS) {
            Log.d("device", "Job scheduled successfully!");
        }
    }

}
