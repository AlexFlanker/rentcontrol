package com.example.alexflanker89.rentcontrol.domain;

import org.greenrobot.greendao.annotation.*;

import java.util.List;
import org.greenrobot.greendao.DaoException;

@Entity(

        active = true,

        nameInDb = "Vehicles",

        createInDb = true,

        generateConstructors = true,

        generateGettersSetters = true
)
public class Vehicle {
    @Id(autoincrement = true)
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String address;

    private boolean onBase;
    @ToMany(referencedJoinProperty = "vehicleId")
    @OrderBy("startTime ASC")
    private List<UseTable> useTable;
/** Used to resolve relations */
@Generated(hash = 2040040024)
private transient DaoSession daoSession;
/** Used for active entity operations. */
@Generated(hash = 900796925)
private transient VehicleDao myDao;
@Generated(hash = 481368525)
public Vehicle(Long id, @NotNull String name, @NotNull String address,
        boolean onBase) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.onBase = onBase;
}
@Generated(hash = 2006430483)
public Vehicle() {
}
public Long getId() {
    return this.id;
}
public void setId(Long id) {
    this.id = id;
}
public String getName() {
    return this.name;
}
public void setName(String name) {
    this.name = name;
}
public String getAddress() {
    return this.address;
}
public void setAddress(String address) {
    this.address = address;
}
public boolean getOnBase() {
    return this.onBase;
}
public void setOnBase(boolean onBase) {
    this.onBase = onBase;
}
/**
 * To-many relationship, resolved on first access (and after reset).
 * Changes to to-many relations are not persisted, make changes to the target entity.
 */
@Generated(hash = 885959782)
public List<UseTable> getUseTable() {
    if (useTable == null) {
        final DaoSession daoSession = this.daoSession;
        if (daoSession == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        UseTableDao targetDao = daoSession.getUseTableDao();
        List<UseTable> useTableNew = targetDao._queryVehicle_UseTable(id);
        synchronized (this) {
            if (useTable == null) {
                useTable = useTableNew;
            }
        }
    }
    return useTable;
}
/** Resets a to-many relationship, making the next get call to query for a fresh result. */
@Generated(hash = 908172330)
public synchronized void resetUseTable() {
    useTable = null;
}
/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 128553479)
public void delete() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.delete(this);
}
/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 1942392019)
public void refresh() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.refresh(this);
}
/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 713229351)
public void update() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.update(this);
}
/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 1588469812)
public void __setDaoSession(DaoSession daoSession) {
    this.daoSession = daoSession;
    myDao = daoSession != null ? daoSession.getVehicleDao() : null;
}

}
